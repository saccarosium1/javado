package entity;

import java.util.ArrayList;

public class Todo {
    boolean state;
    protected String priority;
    protected String description;
    protected ArrayList<String> contexts;
    protected String project;
    protected final String filePath;
    protected final int line;

    public Todo(String filePath, int line) {
        this.description = "";
        this.contexts = new ArrayList<>();
        this.project = "";
        this.priority = "";
        this.filePath = filePath;
        this.line = line;
    }

    public void setState(boolean state) { this.state = state; }
    public boolean getState() { return state; };

    public void setDescription(String description) { this.description = description; }
    public String getDescription() { return description; }
    public void appendToDescription(String s) { description += s; }

    public void setContexts(ArrayList<String> contexts) { this.contexts = contexts; }
    public void addContext(String s) { contexts.add(s); }
    public ArrayList<String> getContexts() { return contexts; }

    public void setProject(String project) { this.project = project; }
    public String getProject() { return this.project; }

    public void setPriority(String priority) { this.priority = priority; }
    public String getPriority() { return this.priority; }

    @Override
    public String toString() {
        return "parser.utils.Todo{" +
                "state=" + state +
                ", description='" + description + '\'' +
                ", contexts=" + contexts +
                ", project='" + project + '\'' +
                ", filePath='" + filePath + '\'' +
                ", line=" + line +
                '}';
    }
}