package entity;

public class Pair<A, B> {
    final protected A first;
    final protected B second;

    public Pair(A first, B second) {
        this.first = first;
        this.second = second;
    }
}