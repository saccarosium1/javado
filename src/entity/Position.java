package entity;

public class Position extends Pair<Integer, Integer> {
    public Position(Integer first, Integer second) { super(first, second); }
    public int getLine() { return second; }
    public int getColumn() { return first; }

    @Override
    public String toString() {
        return String.format("[%d:%d]", getLine(), getColumn());
    }
}