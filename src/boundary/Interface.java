package boundary;

import entity.Todo;
import control.TodoList;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class Interface {
    final int PADDING = 1;
    final int MARGIN = PADDING * 2;
    LinkedHashMap<String, Integer> fields;
    TodoList todos;

    public Interface(TodoList todos) {
        this.todos = todos;
        fields = new LinkedHashMap<>();
        fields.put("Done", 4);
        fields.put("Priority", 8);
        fields.put("Description", 11);
        fields.put("Project", 7);
        fields.put("Contexts", 8);
    }

    private void updateField(String key, int n) {
        fields.replace(key, Integer.max(fields.get(key), n));
    }

    private void adjustFieldSize() {
        updateField("Description", todos.getLongestDescription().length());
        int longestProject = todos.getLongestProject().length();
        if (longestProject > 0) updateField("Project", longestProject);
        else fields.remove("Project");
        boolean noPriorities = todos.getPriorities().toList().isEmpty();
        if (noPriorities) fields.remove("Priority");
        int spaces = todos.getBiggestContexts().size() - 1;
        if (spaces > 0) updateField("Contexts", todos.getBiggestContextsSize() + spaces);
        else fields.remove("Contexts");
    }

    private void printHeader() {
        StringBuilder out = new StringBuilder();
        out.append("+");
        fields.forEach((_, y) -> out.append("-".repeat(y + MARGIN)).append("+"));
        out.append("\n|");
        fields.forEach((x, y) -> out.append(" ".repeat(PADDING)).append(x).append(" ".repeat(y - x.length() + PADDING)).append("|"));
        out.append("\n+");
        fields.forEach((_, y) -> out.append("-".repeat(y + MARGIN)).append("+"));
        System.out.println(out);
    }

    private void printTodo(Todo todo) {
        StringBuilder out = new StringBuilder();
        for (Map.Entry<String, Integer> entry : fields.entrySet()) {
            String field = entry.getKey();
            Integer length = entry.getValue();
            out.append("|");
            out.append(" ".repeat(PADDING));
            switch (field) {
                case "Done":
                    String mark = todo.getState() ? "XXXX" : "";
                    out.append(mark).append(" ".repeat((length + PADDING) - (mark.length())));
                    break;
                case "Priority":
                    String priority = todo.getPriority();
                    out.append(priority).append(" ".repeat((length + PADDING) - (priority.length())));
                    break;
                case "Description":
                    String description = todo.getDescription();
                    out.append(description).append(" ".repeat((length + PADDING) - description.length()));
                    break;
                case "Project":
                    String project = todo.getProject();
                    out.append(project).append(" ".repeat((length + PADDING) - project.length()));
                    break;
                case "Contexts":
                    int sum = 0;
                    ArrayList<String> contexts = todo.getContexts();
                    for (int i = 0; i < contexts.size(); ++i) {
                        out.append(contexts.get(i));
                        sum += contexts.get(i).length();
                        if (i < contexts.size() - 1) {
                            out.append(" ");
                            ++sum;
                        }
                    }
                    out.append(" ".repeat((length + PADDING) - sum));
            }
        }
        out.append("|");
        System.out.println(out);
    }

    private void printFooter() {
        StringBuilder out = new StringBuilder();
        out.append("+");
        fields.forEach((_, y) -> out.append("-".repeat(y + MARGIN)).append("+"));
        System.out.println(out);
    }

    public void start() {
        adjustFieldSize();
        printHeader();
        todos.forEach(this::printTodo);
        printFooter();
    }
}