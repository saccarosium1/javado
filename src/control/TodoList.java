package control;

import entity.Todo;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.function.Function;
import java.util.stream.Stream;

public class TodoList extends ArrayList<Todo> {
    public TodoList(ArrayList<Todo> todos) {
        addAll(todos.stream().toList());
    }

    private String getLongestString(Function<Todo, String> func) {
        String res = "";
        for (Todo todo : this) {
            String desc = func.apply(todo);
            res = res.length() < desc.length() ? desc : res;
        }
        return res;
    }

    public Stream<String> getPriorities() { return this.stream().map(Todo::getPriority).filter(x -> !x.isEmpty()); }

    public Stream<String> getDescriptions() { return this.stream().map(Todo::getDescription); }
    public String getLongestDescription() { return getLongestString(Todo::getDescription); }

    public Stream<String> getProjects() { return this.stream().map(Todo::getProject); }

    public String getLongestProject() {
        return getLongestString(Todo::getProject);
    }

    public Stream<ArrayList<String>> getContexts() {
        return this.stream().map(Todo::getContexts);
    }

    public ArrayList<String> getBiggestContexts() {
        return getContexts()
                .max(Comparator.comparingInt(ArrayList::size))
                .get();
    }

    public Integer getBiggestContextsSize() {
        return getContexts()
                .map(x -> x.stream().mapToInt(String::length).sum())
                .max(Comparator.naturalOrder())
                .get();
    }
}
