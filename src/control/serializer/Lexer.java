package control.serializer;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.function.Predicate;

public class Lexer {
    private String filePath;
    private String fileContent;

    private int line;
    private int cursor;
    private int lastEOL;

    public Lexer(String pathToFile) {
        line = 1;
        cursor = 1;

        try {
            processFile(new File(pathToFile));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public int getColumn() { return cursor - lastEOL; }
    public boolean hasFinished() { return cursor >= fileContent.length(); }

    private void processFile(File file) throws FileNotFoundException {
        if (!file.exists())
            throw new FileNotFoundException("File Don't exists");

        this.filePath = file.getAbsolutePath();

        Scanner scanner = new Scanner(file);
        StringBuilder tmp = new StringBuilder();
        while (scanner.hasNextLine()) {
            tmp.append('\n');
            tmp.append(scanner.nextLine());
        }

        this.fileContent = tmp.toString();
    }

     Token m_lex(Predicate<Character> predicate) {
        StringBuilder tokenString = new StringBuilder();
        char c = fileContent.charAt(cursor);

        int startLine = line;
        int startColumn = getColumn();

        while(!hasFinished() && predicate.test(c)) {
            tokenString.append(c);
            ++cursor;
            if (!hasFinished())
                c = fileContent.charAt(cursor);
        }

        int endLine = line;
        int endColumn = getColumn() - 1;

        return new Token(filePath, tokenString.toString(), startLine, startColumn, endLine, endColumn);
    }

    public Token lexEOL() {
        int startLine = line;
        int startColumn = getColumn();
        int endLine = line++;
        int endColumn = getColumn() + 1;
        lastEOL = cursor++;
        return new Token(filePath, "\n", startLine, startColumn, endLine, endColumn);
    }

    public Token getToken() {
        return switch (fileContent.charAt(cursor)) {
            case '\n' -> lexEOL();
            case ' ' -> m_lex(c -> (c == ' '));
            default -> m_lex((c) -> (c != ' ' && c != '\n'));
        };
    }

    public ArrayList<Token> lex() {
        ArrayList<Token> tokens = new ArrayList<>();
        while (!hasFinished())
            tokens.add(getToken());
        tokens.add(lexEOL());
        return tokens;
    }
}