package control.serializer;

import entity.Todo;
import control.TodoList;

import java.util.ArrayList;

public class Serializer {
    private static String serializeTodo(Todo todo) {
            StringBuilder string = new StringBuilder();
            string.append(todo.getState() ? "X " : "")
                    .append(todo.getDescription())
                    .append(" ")
                    .append(todo.getProject());

            for (String x : todo.getContexts())
                string.append(" ").append(x);

            return string.toString();
        }

    public static ArrayList<String> serialize(ArrayList<Todo> todos) {
        ArrayList<String> res = new ArrayList<>();
        for (Todo x : todos)
            res.add(serializeTodo(x));
        return res;
    }

    public static TodoList deserialize(String file) {
        Lexer lexer = new Lexer(file);
        ArrayList<Token> tokens = lexer.lex();
        return new TodoList(Parser.parse(tokens));
    }
}