package control.serializer;

import entity.Todo;

import java.util.ArrayList;

public class Parser {
    public static ArrayList<Todo> parse(ArrayList<Token> tokens) {
        ArrayList<Todo> todos = new ArrayList<>();

        Todo todo = null;
        for (int i = 0; i < tokens.size(); ++i) {
            Token token = tokens.get(i);

            Token prev_token = (i - 1 >= 0) ? tokens.get(i - 1) : null;
            Token next_token = (i + 1 < tokens.size()) ? tokens.get(i + 1) : null;

            if (todo == null)
                todo = new Todo(token.getFilePath(), token.getStartingLine());

            switch (token.type) {
                case DONE_MARK:
                    todo.setState(true);
                    break;
                case PROJECT_TAG:
                    todo.setProject(token.getString());
                    break;
                case PRIORITY:
                    todo.setPriority(token.getString());
                    break;
                case CONTEXT_TAG:
                    todo.addContext(token.getString());
                    break;
                case WORD:
                    todo.appendToDescription(token.string);
                    break;
                case WHITE_SPACE:
                    if (next_token != null && prev_token != null) {
                        if (prev_token.type == Token.Type.WORD && next_token.type == Token.Type.WORD)
                            todo.appendToDescription(token.string);
                    }
                    break;
                case EOL:
                    todos.add(todo);
                    todo = null;
                    break;
            }
        }
        return todos;
    }

}