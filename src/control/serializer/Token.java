package control.serializer;

import entity.Position;

public class Token {
    public enum Type {
        WORD,
        DONE_MARK,
        PRIORITY,
        WHITE_SPACE,
        PROJECT_TAG,
        CONTEXT_TAG,
        EOL;
    }

    final protected Type type;
    protected String filePath;
    protected String string;
    protected Position start;
    protected Position end;

    public Token(String filePath, String string, int startLine, int startColumn, int endLine, int endColumn) {
        if (startColumn == 1 && (string.equals("x") || string.equals("X")))
            this.type = Type.DONE_MARK;
        else if (string.length() == 3 && string.matches("\\([A-Z]\\)") && (startColumn >= 0 || startColumn <= 3)) {
            this.type = Type.PRIORITY;
        } else {
            this.type = switch (string.charAt(0)) {
                case '\n' -> Type.EOL;
                case '+' -> Type.PROJECT_TAG;
                case '@' -> Type.CONTEXT_TAG;
                case ' ' -> Type.WHITE_SPACE;
                default -> Type.WORD;
            };
        }

        this.filePath = filePath;
        this.string = string != null ? string : "";
        this.start = new Position(startLine, startColumn);
        this.end = new Position(endLine, endColumn);
    }

    public int getStartingLine() { return this.start.getLine(); }
    public int getStartingColumn() { return this.start.getColumn(); }

    public int getEndingLine() { return this.end.getLine(); }
    public int getEndingColumn() { return this.end.getColumn(); }

    public String getFilePath() { return filePath; }
    public String getString() { return string; }

    @Override
    public String toString() {
        return "{" + '\n' +
                "  type: " + this.type.name() + '\n' +
                "  file: " + this.filePath + '\n' +
                "  string: " + String.format("'%s'", this.string.equals("\n") ? "EOL" : this.string) + '\n' +
                "  starts: " + this.start + '\n' +
                "  ends: " + this.end + '\n' +
                "}";
    }
}